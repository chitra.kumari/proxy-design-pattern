package protectionProxy;// In this situation the proxy both creates and destroys
// an protectionProxy.ATMMachine Object

public class ATMProxy implements GetATMData {

    // Allows the user to access getATMState in the
    // Object protectionProxy.ATMMachine

    public ATMState getATMState() {

        ATMMachine realATMMachine = new ATMMachine();

        return realATMMachine.getATMState();
    }

    // Allows the user to access getCashInMachine
    // in the Object protectionProxy.ATMMachine

    public int getCashInMachine() {

        ATMMachine realATMMachine = new ATMMachine();

        return realATMMachine.getCashInMachine();

    }

}