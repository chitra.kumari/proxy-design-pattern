package protectionProxy;

public interface ATMState {

    // Different states expected
    // protectionProxy.HasCard, protectionProxy.NoCard, protectionProxy.HasPin, protectionProxy.NoCash

    void insertCard();

    void ejectCard();

    void insertPin(int pinEntered);

    void requestCash(int cashToWithdraw);

}